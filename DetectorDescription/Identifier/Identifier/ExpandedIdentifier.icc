/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

//-----------------------------------------------
inline ExpandedIdentifier::ExpandedIdentifier(const ExpandedIdentifier& other,
    size_type start){
  if (start < other.fields()) {
    element_vector::const_iterator it = other.m_fields.begin();
    it += start;
    m_fields.insert(m_fields.end(), it, other.m_fields.end());
  }
}

// Modifications
inline void
ExpandedIdentifier::add(element_type value){
  // Max size of id levels should be < 12
  m_fields.push_back(value);
}

inline ExpandedIdentifier&
ExpandedIdentifier::operator<<(element_type value){
  // Max size of id levels should be < 12
  m_fields.push_back(value);
  return (*this);
}

inline ExpandedIdentifier::element_type& ExpandedIdentifier::operator[](
  size_type index){
  // Raises an exception if index is out-of-bounds.
  return m_fields.at(index);
}

inline void
ExpandedIdentifier::clear(){
  m_fields.clear();
}

inline ExpandedIdentifier::element_type ExpandedIdentifier::operator[](
  size_type index) const{
  // Raises an exception if index is out-of-bounds.
  return m_fields.at(index);
}

inline ExpandedIdentifier::size_type
ExpandedIdentifier::fields() const{
  return (m_fields.size());
}

// Comparison operators
inline bool
ExpandedIdentifier::operator==(const ExpandedIdentifier& other) const{
  if (fields() != other.fields()) return false;
  return (m_fields == other.m_fields);
}

inline auto ExpandedIdentifier::operator <=>( const ExpandedIdentifier& other) const{
    return std::lexicographical_compare_three_way(m_fields.begin(), m_fields.end(),
      other.m_fields.begin(), other.m_fields.end());
}


inline bool
ExpandedIdentifier::match(const ExpandedIdentifier& other) const{
  const ExpandedIdentifier& me = *this;
  const size_type my_fields = fields();
  const size_type other_fields = other.fields();
  const size_type fs = (my_fields < other_fields) ? my_fields : other_fields;
  for (size_type field = 0; field < fs; ++field) {
    if (me[field] != other[field])
      return false;
  }
  return true;
}

